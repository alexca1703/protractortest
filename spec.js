// spec.js

let homepage = require('./page/homepage');

describe('Protractor Demo App', function() {
  
    
    const projectURL = {url:"http://juliemr.github.io/protractor-demo/"};
    
    it('should have a title', function() {
    browser.get(projectURL.url);
    homepage.verifyResultTitle('Super Calculator');
    browser.sleep(2000);
    });

    it('should perform additions', function() {
         homepage.getURLProject(projectURL.url);
         homepage.enterFirstNumberValue('5');
         homepage.enterSecondNumberValue('3');
         homepage.clickButtonGo();
         browser.sleep(2000);
    });    
 
    it('should show the addition result', function(){
      homepage.verifyResult('8');
      browser.sleep(5000);
    });

  });
