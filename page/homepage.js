let HomePage = function(){
    let firstNumber_input = element(by.model('first'));
    let secondNumber_input = element(by.model('second'));
    let goButton = element(by.css('[ng-click="doAddition()"]'));
    
    this.getURLProject = function(url) {
         browser.get(url);
    }

    this.enterFirstNumberValue = function (firstNumberValue){
        firstNumber_input.sendKeys(firstNumberValue);
    }

    this.enterSecondNumberValue = function (SecondNumberValue){
        secondNumber_input.sendKeys(SecondNumberValue);
    }

    this.clickButtonGo = function (SecondNumberValue){
        goButton.click();
    }

    this.verifyResultTitle = function (resultTitle){
        expect(resultTitle).toEqual('Super Calculator');
    }

    this.verifyResult = function (result){
        let output = element(by.cssContainingText('ng-binding', result));
        expect(output.getText()).toEqual(result);
    }
};

module.exports = new HomePage();